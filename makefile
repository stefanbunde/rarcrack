run:
	rarcrack -v tests/sample.rar 0123456
	rm sample.txt

build:
	python setup.py sdist

install:
	pip install -e .
