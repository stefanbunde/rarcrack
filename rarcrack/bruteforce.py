import itertools


def bruteforce(charset, max_length=12, start_value=""):
    charset = prepare_charset(charset)
    for length in range(1, max_length + 1):
        for candidate in get_permutations(charset, length, start_value):
            yield candidate


def prepare_charset(charset):
    return "".join(sorted(set(charset)))


def get_permutations(charset, length, start_value=""):
    for item in itertools.product(charset, repeat=length):
        permutation = "".join(item)
        if len(permutation) < len(start_value) or \
                (len(permutation) == len(start_value) and permutation < start_value):
            continue
        yield permutation
