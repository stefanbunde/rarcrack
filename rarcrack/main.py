import argparse

from rarcrack.rarcrack import RarCrack


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("rarfile", help="rar file to crack")
    parser.add_argument("charset", help="specify the charset")
    parser.add_argument("--start_with", default="", metavar="string",
                        help="if the last run was interrupted, continue with this string")
    parser.add_argument("-v", "--verbose", action="store_true", help="increase verbosity")
    args = parser.parse_args()

    r = RarCrack(args.rarfile, args.charset, args.start_with, args.verbose)
    r.run()


if __name__ == "__main__":
    main()
