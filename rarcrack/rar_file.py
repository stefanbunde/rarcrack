from rarfile import RarNoFilesError


class PasswordCracked(Exception):
    pass


def extract(file_handle, password):
    try:
        file_handle.extractall(pwd=password)
        raise PasswordCracked("password: {}".format(password))
    except RarNoFilesError:
        return
