from threading import Semaphore, Thread

from rarfile import RarFile

from rarcrack.bruteforce import bruteforce
from rarcrack.rar_file import extract, PasswordCracked


semaphore = Semaphore(4)


class Attempt(Thread):
    def __init__(self, rarfile, chars, password_cracked_callback):
        Thread.__init__(self)
        self.rarfile = rarfile
        self.chars = chars
        self.password_cracked_callback = password_cracked_callback

    def run(self):
        try:
            extract(self.rarfile, self.chars)
        except PasswordCracked as e:
            print(e)
            self.password_cracked_callback()
        finally:
            semaphore.release()


class RarCrack(object):
    def __init__(self, rar_filename, charset, start_with="", verbose=False):
        self.rar_filename = rar_filename
        self.charset = charset
        self.start_with = start_with
        self.verbose = verbose

        self.password_cracked = False

    def run(self):
        rarfile = RarFile(self.rar_filename)

        for count, attempt in enumerate(bruteforce(self.charset, start_value=self.start_with)):
            semaphore.acquire()
            a = Attempt(rarfile, attempt, self.password_cracked_callback)
            a.start()

            self.print_attempt(count, attempt)

            if self.password_cracked:
                return

    def print_attempt(self, count, attempt):
        if self.verbose:
            print(attempt)
        elif count % 100 == 0:
            print(attempt)

    def password_cracked_callback(self):
        self.password_cracked = True
