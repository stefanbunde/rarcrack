from setuptools import find_packages, setup


def get_file_content(filename):
    with open(filename) as fh:
        return fh.read()


setup(
    name="rarcrack",
    version=get_file_content("VERSION"),
    packages=find_packages(),
    author=get_file_content("contributors.txt"),
    author_email="stefanbunde+git@gmail.com",
    entry_points={
        "console_scripts": [
            "rarcrack=rarcrack.main:main",
        ],
    },
    install_requires=[
        "rarfile==3.0",
    ],
)
