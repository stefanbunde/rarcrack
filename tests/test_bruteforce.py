from rarcrack.bruteforce import bruteforce, get_permutations, prepare_charset


def test_prepare_charset():
    assert "abcd" == prepare_charset("dcab")


def test_prepare_charset_with_duplicates():
    assert "abcd" == prepare_charset("dabdabc")


def test_get_permutations():
    assert ["a", "b"] == list(get_permutations("ab", 1))
    assert ["aa", "ab", "ba", "bb"] == list(get_permutations("ab", 2))
    assert ["aaa", "aab", "aba", "abb",
            "baa", "bab", "bba", "bbb"] == list(get_permutations("ab", 3))


def test_get_permutations_with_start_value():
    start_value = "abb"
    assert ["abb", "baa", "bab", "bba", "bbb"] == list(get_permutations("ab", 3, start_value))


def test_bruteforce():
    result = list(bruteforce("ab", 3))
    assert ["a", "b", "aa", "ab", "ba", "bb",
            "aaa", "aab", "aba", "abb", "baa", "bab", "bba", "bbb"] == result


def test_bruteforce_with_start_value():
    result = list(bruteforce("ab", 3, "ba"))
    assert ["ba", "bb", "aaa", "aab", "aba", "abb", "baa", "bab", "bba", "bbb"] == result
