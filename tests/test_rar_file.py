import pytest
from rarfile import RarFile

from rarcrack.rar_file import extract, PasswordCracked


@pytest.fixture
def rarfile():
    return RarFile("tests/sample.rar")


def test_extract_with_incorrect_password(rarfile):
    result = extract(rarfile, "111")
    assert result is None


def test_extract_with_correct_password(rarfile):
    with pytest.raises(PasswordCracked) as exception:
        extract(rarfile, "123")
        assert "password: 123" == exception.msg
