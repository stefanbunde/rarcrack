import pytest

from rarcrack.rarcrack import RarCrack


def test_run(capsys):
    rarcrack = RarCrack("tests/sample.rar", "0123")
    rarcrack.run()

    captured = capsys.readouterr()
    assert "0\npassword: 123\n" == captured.out


def test_run_with_start_value(capsys):
    rarcrack = RarCrack("tests/sample.rar", "0123", "100")
    rarcrack.run()

    captured = capsys.readouterr()
    assert "100\npassword: 123\n" == captured.out


@pytest.mark.parametrize("count, charset, expected", [
    (1, "aba", ""),
    (2, "abb", ""),
    (3, "abc", ""),
    (99, "cca", ""),
    (100, "ccb", "ccb\n"),
    (101, "ccc", ""),
])
def test_print_attempt_non_verbose(capsys, count, charset, expected):
    rarcrack = RarCrack("", "", "")

    rarcrack.print_attempt(count, charset)
    captured = capsys.readouterr()
    assert expected == captured.out


@pytest.mark.parametrize("count, charset", [
    (1, "aba"),
    (2, "abb"),
    (3, "abc"),
    (99, "cca"),
    (100, "ccb"),
    (101, "ccc"),
])
def test_print_attempt_verbose(capsys, count, charset):
    rarcrack = RarCrack("", "", "", True)
    expected = "{}\n".format(charset)

    rarcrack.print_attempt(count, charset)
    captured = capsys.readouterr()
    assert expected == captured.out
